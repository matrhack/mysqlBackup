#!/bin/sh

echo -e -n "\n --> Sql HOST : "
read DB_HOST

echo -e -n "\n --> Sql USER : "
read DB_USER

echo -e -n "\n --> Sql PWD: "
stty -echo
read DB_PASS
stty echo

echo -e -n "\n ==> Export or Import ?"
read CHOICE

if [ "$CHOICE" == "e" ]
then

    # sous-chemin de destination
    OUTDIR="./bddBackup_`date +%Y_%m_%d__%H_%M_%S`"

    # création de l'arborescence
    #mkdir -p ./sql/$OUTDIR

    # récupération de la liste des bases
    DATABASES=`MYSQL_PWD=$DB_PASS mysql -h $DB_HOST -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql`

    # boucle sur les bases pour les dumper
    for DB_NAME in $DATABASES; do
        echo -e -n "\n----> Export $DB_NAME ? y/n (default:y)"
        read TEST
        if [ $TEST != "n" ]
        then
            MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB_NAME -h $DB_HOST > "$OUTDIR_$DB_NAME.sql"
        fi
    done

    echo -e -n "\n---> Zip all files ? y/n (default:n)"
    read TEST
    if [ "$TEST" == "y" ]
    then
        # boucle sur les bases pour compresser les fichiers
        for filename in ./*.sql; do
            gzip "$filename"
            rm "$filename"
        done
    fi
fi
if [ "$CHOICE" == "i" ]
then
    DATABASES=`MYSQL_PWD=$DB_PASS mysql -h $DB_HOST -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql`

    # boucle sur les bases pour compresser les fichiers
    for filename in ./*.sql; do
    
        echo -e -n "\n ---> $DATABASES"
        echo -e -n "\n ---> import $filename where ? (default:n)"
        read DB
        if [ "$DB" != "n" ]
        then
            MYSQL_PWD=$DB_PASS mysql -h $DB_HOST -u $DB_USER $DB < $filename
            echo -e -n "\n ---> remove file y/n (default:y)"
            read RM
            if [ "$RM" != "n" ]
            then
                rm $filename
            fi
        fi
    done
fi
